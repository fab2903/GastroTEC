# GastroTEC

This project is the first short assesment for the course SOA4ID of Instituto Tecnológico de Costa Rica for the II Semester 2018. This application will allow people incharge of a "restaurant" to publish the menu for the different schedules they offer.

## General Instructions

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

```
For better results and experience, this app was designed for Android 8.0.0, API 26.
```

### Prerequisites

For a better experience, it is recommended to have the following:

```
Android Studio
Android Phone, with internet connection
```

### Installing

The following instructions will guide you to install this project on your local machine and work on it using Android Studio. Additionally, it will give you the steps on how to install the app in your phones. 

Copy the Gitlab repository URL:

```
https://gitlab.com/fab2903/GastroTEC.git
```

Then, open Android Studio, and go to 

```
File-> New-> Project from Version Control-> Git
```

On the new screen, paste the Git Repository URL and select a local folder where the project will be saved, and finally give it a name.  
Example:

```
https://gitlab.com/fabsolano2903/GastroTEC.git
C:\Users\me\Desktop
GastroTEC
```

Once the project is finally downloaded and opened, you need to Buid it. 
You can press F9, press the "hammer" icon located on the top mid-right or go to:

```
 Build-> Make Project
```

Finally, to install the app in your phone you will need to press Shift+F10, press the "play" icon  located on the top mid-right or go to:

```
 Run-> Run 'app'
```
### Testing

To test the admin mode, create an user with the email admin@admin.com.
Also, after creating a new restaurant to be able to see the change right away rotate the phone. This will make the new restaurant appear in the list.



## Built With

* [Android Studio](https://developer.android.com/studio/) - The IDE used

## Authors

* **Fabian Solano** - [Fab2903](https://gitlab.com/fab2903) 


## Acknowledgments

* Lalit V. Android Login and Register
* Android Studio Android developer guides


