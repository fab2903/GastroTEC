package com.fabiansolano.secondapp.gastrotec.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.os.AsyncTask;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import java.util.ArrayList;
import java.util.List;

import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.Toast;


import com.fabiansolano.secondapp.gastrotec.R;
import com.fabiansolano.secondapp.gastrotec.adapters.RestaurantsAdapter;
import com.fabiansolano.secondapp.gastrotec.model.Restaurant;
import com.fabiansolano.secondapp.gastrotec.sql.DatabaseHelper;


public class RestaurantsActivity extends AppCompatActivity {

    private AppCompatActivity activity = RestaurantsActivity.this;
    private AppCompatTextView textViewName;
    private RecyclerView recyclerViewRestaurants;

    private List<Restaurant> listRestaurants;
    private RestaurantsAdapter restaurantsAdapter;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants_list);
        initViews();
        initObjects();

    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        textViewName = (AppCompatTextView) findViewById(R.id.textViewName);
        recyclerViewRestaurants = (RecyclerView) findViewById(R.id.recyclerViewRestaurants);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        listRestaurants = new ArrayList<>();
        restaurantsAdapter = new RestaurantsAdapter(listRestaurants);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewRestaurants.setLayoutManager(mLayoutManager);
        recyclerViewRestaurants.setItemAnimator(new DefaultItemAnimator());
        recyclerViewRestaurants.setHasFixedSize(true);
        recyclerViewRestaurants.setAdapter(restaurantsAdapter);
        databaseHelper = new DatabaseHelper(activity);

        String emailFromIntent = getIntent().getStringExtra("EMAIL");
        textViewName.setText(emailFromIntent);


        getDataFromSQLite();
    }

    /**
     * This method is to fetch all user records from SQLite
     */
    private void getDataFromSQLite() {
        // AsyncTask is used that SQLite operation not blocks the UI Thread.
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                listRestaurants.clear();
                listRestaurants.addAll(databaseHelper.getAllRestaurants());

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                restaurantsAdapter.notifyDataSetChanged();
            }
        }.execute();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String admin = "admin@admin.com";

        String emailFromIntent = getIntent().getStringExtra("EMAIL");

        if (emailFromIntent.contentEquals(admin)){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {


        case R.id.add:
            // Navigate to RegisterActivity
            Intent intentRegister = new Intent(getApplicationContext(), RegisterRestaurantActivity.class);
            startActivity(intentRegister);
            break;
        case R.id.about:
            Toast.makeText(this, R.string.about_toast, Toast.LENGTH_LONG).show();
            return(true);
        case R.id.exit:
            finish();
            return(true);
    }
        return(super.onOptionsItemSelected(item));
    }

}