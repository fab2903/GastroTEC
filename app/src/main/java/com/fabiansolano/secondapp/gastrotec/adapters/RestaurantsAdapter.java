package com.fabiansolano.secondapp.gastrotec.adapters;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fabiansolano.secondapp.gastrotec.R;
import com.fabiansolano.secondapp.gastrotec.model.Restaurant;


import java.util.List;


public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.RestaurantViewHolder> {

    private List<Restaurant> listRestaurants;

    public RestaurantsAdapter(List<Restaurant> listRestaurants) {
        this.listRestaurants = listRestaurants;
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_restaurant, parent, false);

        return new RestaurantViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, int position) {
        holder.textViewRestaurantName.setText(listRestaurants.get(position).getName());
        holder.textViewRestaurantAddress.setText(listRestaurants.get(position).getAddress());
        holder.textViewRestaurantSchedule.setText(listRestaurants.get(position).getSchedule());
    }

    @Override
    public int getItemCount() {
        Log.v(RestaurantsAdapter.class.getSimpleName(),""+listRestaurants.size());
        return listRestaurants.size();
    }


    /**
     * ViewHolder class
     */
    public class RestaurantViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView textViewRestaurantName;
        public AppCompatTextView textViewRestaurantAddress;
        public AppCompatTextView textViewRestaurantSchedule;

        public RestaurantViewHolder(View view) {
            super(view);
            textViewRestaurantName = (AppCompatTextView) view.findViewById(R.id.textViewRestaurantName);
            textViewRestaurantAddress = (AppCompatTextView) view.findViewById(R.id.textViewRestaurantAddress);
            textViewRestaurantSchedule = (AppCompatTextView) view.findViewById(R.id.textViewRestaurantSchedule);
        }
    }


}