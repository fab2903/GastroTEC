package com.fabiansolano.secondapp.gastrotec.activities;



import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.fabiansolano.secondapp.gastrotec.R;
import com.fabiansolano.secondapp.gastrotec.helper.InputValidation;
import com.fabiansolano.secondapp.gastrotec.model.Restaurant;
import com.fabiansolano.secondapp.gastrotec.sql.DatabaseHelper;

public class RegisterRestaurantActivity extends AppCompatActivity implements View.OnClickListener  {

    private final AppCompatActivity activity = RegisterRestaurantActivity.this;
    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutRestaurantName;
    private TextInputLayout textInputLayoutRestaurantAddress;
    private TextInputLayout textInputLayoutRestaurantSchedule;
    private TextInputLayout textInputLayoutRestaurantSchedulefood;
    private TextInputLayout textInputLayoutRestaurantDish;
    private TextInputLayout textInputLayoutRestaurantVote;

    private TextInputEditText textInputEditTextRestaurantName;
    private TextInputEditText textInputEditTextRestaurantAddress;
    private TextInputEditText textInputEditTextRestaurantSchedule;
    private TextInputEditText textInputEditTextRestaurantSchedulefood;
    private TextInputEditText textInputEditTextRestaurantDish;
    private TextInputEditText textInputEditTextRestaurantVote;

    private AppCompatButton appCompatButtonRegisterRestaurant;
    private AppCompatTextView appCompatTextViewCancel;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private Restaurant restaurant;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_register);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutRestaurantName = (TextInputLayout) findViewById(R.id.textInputLayoutRestaurantName);
        textInputLayoutRestaurantAddress = (TextInputLayout)findViewById(R.id.textInputLayoutRestaurantAddress);
        textInputLayoutRestaurantSchedule = (TextInputLayout) findViewById(R.id.textInputLayoutRestaurantSchedule);
        textInputLayoutRestaurantSchedulefood = (TextInputLayout) findViewById(R.id.textInputLayoutRestaurantSchedulefood);
        textInputLayoutRestaurantDish = (TextInputLayout) findViewById(R.id.textInputLayoutRestaurantDish);
        textInputLayoutRestaurantVote = (TextInputLayout) findViewById(R.id.textInputLayoutRestaurantVote);

        textInputEditTextRestaurantName = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantName);
        textInputEditTextRestaurantAddress = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantAddress);
        textInputEditTextRestaurantSchedule = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantSchedule);
        textInputEditTextRestaurantSchedulefood = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantSchedulefood);
        textInputEditTextRestaurantDish = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantDish);
        textInputEditTextRestaurantVote = (TextInputEditText) findViewById(R.id.textInputEditTextRestaurantVote);

        appCompatButtonRegisterRestaurant = (AppCompatButton) findViewById(R.id.appCompatButtonRegisterRestaurant);

        appCompatTextViewCancel = (AppCompatTextView) findViewById(R.id.appCompatTextViewCancel);

    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonRegisterRestaurant.setOnClickListener(this);
        appCompatTextViewCancel.setOnClickListener(this);

    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        restaurant = new Restaurant();

    }


    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.appCompatButtonRegisterRestaurant:
                postDataToSQLite();
                emptyInputEditText();
                break;

            case R.id.appCompatTextViewCancel:
                finish();
                emptyInputEditText();
                break;
        }
    }

    /**
     * This method is to validate the input text fields and post data to SQLite
     */
    private void postDataToSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantName, textInputLayoutRestaurantName, getString(R.string.error_message_restaurant_name))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantAddress, textInputLayoutRestaurantAddress, getString(R.string.error_message_restaurant_address))) {
            return;
        }

        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantSchedule, textInputLayoutRestaurantSchedule, getString(R.string.error_message_restaurant_schedule))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantSchedulefood, textInputLayoutRestaurantSchedulefood, getString(R.string.error_message_restaurant_schedulefood))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantDish, textInputLayoutRestaurantDish, getString(R.string.error_message_restaurant_dish))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextRestaurantVote, textInputLayoutRestaurantVote, getString(R.string.error_message_restaurant_vote))) {
            return;
        }


        if (!databaseHelper.checkRestaurant(textInputEditTextRestaurantName.getText().toString().trim())){

            restaurant.setName(textInputEditTextRestaurantName.getText().toString().trim());
            restaurant.setAddress(textInputEditTextRestaurantAddress.getText().toString().trim());
            restaurant.setSchedule(textInputEditTextRestaurantSchedule.getText().toString().trim());
            restaurant.setSchedulefood(textInputEditTextRestaurantSchedulefood.getText().toString().trim());
            restaurant.setDish(textInputEditTextRestaurantDish.getText().toString().trim());
            restaurant.setVote(textInputEditTextRestaurantVote.getText().toString().trim());

            databaseHelper.addRestaurant(restaurant);

            // Snack Bar to show success message that record saved successfully
            Snackbar.make(nestedScrollView, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
            emptyInputEditText();


        } else {
            // Snack Bar to show error message that record already exists
            Snackbar.make(nestedScrollView, getString(R.string.error_message_restaurant), Snackbar.LENGTH_LONG).show();
        }
    }
    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextRestaurantName.setText(null);
        textInputEditTextRestaurantAddress.setText(null);
        textInputEditTextRestaurantSchedule.setText(null);
        textInputEditTextRestaurantSchedulefood.setText(null);
        textInputEditTextRestaurantDish.setText(null);
        textInputEditTextRestaurantVote.setText(null);
    }
}

