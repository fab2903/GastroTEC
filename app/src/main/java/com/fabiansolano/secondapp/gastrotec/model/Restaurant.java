package com.fabiansolano.secondapp.gastrotec.model;

public class Restaurant {


    private int id;
    private String name;
    private String address;
    private String schedule;
    private String schedulefood;
    private String dish;
    private String vote;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getSchedulefood() {
        return schedulefood;
    }

    public void setSchedulefood(String schedulefood) {
        this.schedulefood = schedulefood;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

}
